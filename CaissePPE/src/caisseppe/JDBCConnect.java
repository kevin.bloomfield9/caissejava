/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caisseppe;

import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Utilisateur
 */
public class JDBCConnect {	
    static String url = "jdbc:mysql://127.0.0.1:3306/caisseppe?useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    static String login = "root";
    static String pw = "";
    
    public static ArrayList produit = new ArrayList();
    public static ArrayList ingredient = new ArrayList();
    
    public static void ListProduit(String sql) throws Exception{
		
	Class.forName("com.mysql.cj.jdbc.Driver");
		
	try (Connection conn = DriverManager.getConnection(url, login, pw)){
	
            try(Statement statement = conn.createStatement()){
		ResultSet rs = statement.executeQuery(sql);
                while(rs.next()){
                    produit.add(0, rs.getString("nom"));
                    produit.add(1, rs.getString("prix"));
                }
            }
        }
    }
    
    public static void ListIngredient(String sql) throws Exception{
		
	Class.forName("com.mysql.cj.jdbc.Driver");
		
	try (Connection conn = DriverManager.getConnection(url, login, pw)){
	
            try(Statement statement = conn.createStatement()){
		ResultSet rs = statement.executeQuery(sql);
                while(rs.next()){
                    ingredient.add(0, rs.getString("nom"));
                }
            }
        }
    }
    
    public static void ValideCommande(String sql) throws Exception{
        Class.forName("com.mysql.cj.jdbc.Driver");
        
	try (Connection conn = DriverManager.getConnection(url, login, pw)){
	
            try(Statement statement = conn.createStatement()){
		statement.executeUpdate(sql);
            }
        }
    }
}