CREATE DATABASE `caisseppe`;
USE `caisseppe`;

CREATE TABLE `commande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `prix` float NOT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `ingredient` (`nom`) VALUES
('Vanille'),
('Chocolat'),
('Chewing Gum'),
('Coco');

CREATE TABLE `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prix` float NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `produit` (`nom`, `prix`) VALUES
('1 Boule',	1),
('2 Boules',	1.8),
('3 Boules',	2.8),
('Milk-Shake',	3.5);